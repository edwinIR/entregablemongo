package mis.entregable.mongdb.datos;

import mis.entregable.mongdb.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProducto extends MongoRepository<Producto,String> {
}
package mis.entregable.mongdb.datos;

import mis.entregable.mongdb.modelo.Producto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioProducto {

    public Producto crearProducto(Producto producto);
    public List<Producto> obtenerProductos();
    public Optional<Producto> obtenerProductoPorId(String id);
    public Producto actualizarProducto(Producto producto);
    public void borrarProductoPorId(String id);
}

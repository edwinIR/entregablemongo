package mis.entregable.mongdb.Controlador;

import mis.entregable.mongdb.datos.ServicioProducto;
import mis.entregable.mongdb.modelo.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v2/productos")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public List<Producto> obtenerProductos(){
        return servicioProducto.obtenerProductos();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Producto crearProducto(@RequestBody Producto producto){
        return servicioProducto.crearProducto(producto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarProducto(@PathVariable String id){
        servicioProducto.borrarProductoPorId(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Producto actualizarProducto(@PathVariable String id,@RequestBody Producto producto){
        return servicioProducto.actualizarProducto(producto);
    }

    /*@GetMapping("/{id}")
    public Producto obtenerProductos(@PathVariable String id){
        final Optional<Producto> p=  servicioProducto.obtenerProductoPorId(id);
        if(p.isPresent()){
            return ResponseEntity.ok(p);
        }
        return  (HttpStatus.NOT_FOUND);
    }*/
}